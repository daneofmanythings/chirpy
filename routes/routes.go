package routes

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/daneofmanythings/chirpy/pkg/config"
	"gitlab.com/daneofmanythings/chirpy/pkg/handlers"
	"gitlab.com/daneofmanythings/chirpy/pkg/middleware"
)

const filepathRoot = "."

func Routes(app *config.Config) http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.MiddlewareCors)

	fsHandler := http.StripPrefix("/app", http.FileServer(http.Dir(filepathRoot)))
	fsHandler = middleware.MiddlewareMetricsInc(fsHandler, app)

	r.Handle("/app/*", fsHandler)
	r.Handle("/app", fsHandler)

	r_api := chi.NewRouter()
	r_api.Get("/healthz", handlers.Repo.HealthHandler)
	r_api.Get("/reset", handlers.Repo.ApiResetMetrics)
	r_api.Post("/validate_chirp", handlers.Repo.ValidateChirpHandler)
	r.Mount("/api", r_api)

	r_admin := chi.NewRouter()
	r_admin.Get("/metrics", handlers.Repo.ApiHitsHandler)
	r.Mount("/admin", r_admin)

	return r
}
