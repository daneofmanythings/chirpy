package main

import (
	"log"
	"net/http"

	"gitlab.com/daneofmanythings/chirpy/pkg/config"
	"gitlab.com/daneofmanythings/chirpy/pkg/handlers"
	"gitlab.com/daneofmanythings/chirpy/routes"
)

const (
	portNumber = ":8080"
)

var app config.Config

func main() {
	app.ResetHits()

	repo := handlers.NewRepo(&app)
	handlers.LinkRepository(repo)

	server := http.Server{
		Addr:    portNumber,
		Handler: routes.Routes(&app),
	}

	log.Fatal(server.ListenAndServe())
}
