package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/daneofmanythings/chirpy/pkg/config"
)

var Repo *Repository

type Repository struct {
	App *config.Config
}

func NewRepo(c *config.Config) *Repository {
	return &Repository{
		App: c,
	}
}

func LinkRepository(r *Repository) {
	Repo = r
}

func (m *Repository) HealthHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("OK"))
}

func (m *Repository) ValidateChirpHandler(w http.ResponseWriter, r *http.Request) {
	type chirpParams struct {
		Body string `json:"body"`
	}

	decoder := json.NewDecoder(r.Body)
	params := chirpParams{}
	err := decoder.Decode(&params)
	if err != nil {
		log.Printf("Error decoding parameters: %s", err)
		w.WriteHeader(500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if len(params.Body) > 140 {
		RespondWithError(w, http.StatusBadRequest, "chirp is too long")
	} else if len(params.Body) == 0 {
		RespondWithError(w, http.StatusBadRequest, "chirp is empty")
	} else {
		RespondWithJSON(w, http.StatusOK, params.Body)
	}
}

func (m *Repository) ApiHitsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	body := `
<html>

<body>
    <h1>Welcome, Chirpy Admin</h1>
    <p>Chirpy has been visited %d times!</p>
</body>

</html>
  `
	w.Write([]byte(fmt.Sprintf(body, m.App.GetHits())))
}

func (m *Repository) ApiResetMetrics(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	m.App.ResetHits()
	w.Write([]byte("Metrics have been reset"))
}
