package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"slices"
	"strings"
)

func RespondWithError(w http.ResponseWriter, code int, msg string) {
	type returnJSON struct {
		Error string
	}

	dat, err := json.Marshal(returnJSON{Error: msg})
	if err != nil {
		log.Printf("Error marshalling JSON: %s", err)
		w.WriteHeader(500)
		return
	}
	w.WriteHeader(code)
	w.Write(dat)
}

func RespondWithJSON(w http.ResponseWriter, code int, payload string) {
	type returnJSON struct {
		CleanedBody string `json:"cleaned_body"`
	}

	censoredPayload := censorChirp(payload)

	dat, err := json.Marshal(returnJSON{CleanedBody: censoredPayload})
	if err != nil {
		log.Printf("Error marshalling JSON: %s", err)
		w.WriteHeader(500)
		return
	}
	w.WriteHeader(code)
	w.Write(dat)
}

func censorChirp(chirp string) string {
	badWords := []string{
		"kerfuffle",
		"sharbert",
		"fornax",
	}
	censorBar := "****"

	splitChirp := strings.Split(chirp, " ")
	for i, word := range splitChirp {
		if slices.Contains(badWords, strings.ToLower(word)) {
			splitChirp[i] = censorBar
		}
	}
	return strings.Join(splitChirp, " ")
}
