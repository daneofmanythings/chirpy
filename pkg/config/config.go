package config

type Config struct {
	fileserverHits int
}

func (c *Config) ResetHits() {
	c.fileserverHits = 0
}

func (c *Config) HitRegistered() {
	c.fileserverHits++
}

func (c *Config) GetHits() int {
	return c.fileserverHits
}
